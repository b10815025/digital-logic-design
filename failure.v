module moore(y_out,x_in,clock,reset);
input x_in,clock,reset;
output reg y_out;
reg [1:0] state,next_state;
parameter a=3'b000,b=3'b001,c=3'b010,d=3'b011,e=3'b100;
always@(posedge clock or negedge reset)begin
if(~reset) state<=a;
else state<=next_state;
end
always@(state or x_in)begin
case(state)
a: if(x_in) next_state=b; else next_state=a;
b: if(x_in) next_state=c; else next_state=b;
c: if(x_in) next_state=d; else next_state=c;
d: if(x_in) next_state=e; else next_state=d;
e: if(x_in) next_state=a; else next_state=e;
endcase
end
always@(state)begin
case(state)
a,b,c,d: y_out=1'b0;
e: y_out=1'b1;
endcase
end
endmodule