module Bin2Dec(Sw,Seg7);
input[8:0]Sw;
output[15:0]Seg7;
wire[3:0] n0,n1;
assign n1=(Sw%100)/10;
assign n0=(Sw%10);
Seg7Decode s0(n0, Seg7[7:0]);
Seg7Decode s1(n1, Seg7[15:8]);
endmodule