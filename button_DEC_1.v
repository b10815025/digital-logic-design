module button1_dec(Rin,but0,but1,but2,scoreout,failedout);
input Rin,but0,but1,but2;
output scoreout,failedout;
reg scoreout,failedout;
integer temp , temp1;
always@ (Rin)
begin
	if(Rin)
		temp = 1;
	else
		temp = 0;
end
always@ (posedge but1)
begin 		
	temp1 = temp;
	failedout = 0;
	scoreout =  0;	
	if(but1&&temp1)
	begin
		scoreout =  1;		
	end
	else
	begin
		failedout = 1;		
	end	
end
endmodule