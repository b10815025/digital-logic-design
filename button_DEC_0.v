module button0_dec(but0In,but1In,but2In,,but0,but1,but2,scoreout,failedout);
input but0In,but1In,but2In,but0,but1,but2;
output[8:0]scoreout,failedout;
reg[8:0] scoreout,failedout;
integer but_0, but_1 , but_2,temp,temp1, temp2;

initial
begin
	temp = 0;
end


always@ (but0In)
begin
	if(but0In)
		temp = 1;
	else
		temp = 0;
end
always@ (but1In)
begin
	if(but1In)
		temp1 = 1;
	else
		temp1 = 0;
end
always@ (but2In)
begin
	if(but2In)
		temp2 = 1;
	else
		temp2 = 0;
end

always@ (posedge but0, posedge but1, posedge but2)
begin 	
	but_0 = temp;
	but_1 = temp1;
	but_2 = temp2;
	if((but0&&temp) == 1)
		begin
			scoreout = scoreout + 1;
		end
	else
		begin
			failedout = failedout + 1;
		end
		
	/*if(but1)
		begin
			if(but_1)
				scoreout = scoreout + 1;
			else
				failedout = failedout + 1;
		end
	if(but2)
		begin 
			if(but_2)
				scoreout = scoreout + 1;
			else
				failedout = failedout + 1;
		end
	*/
end
endmodule