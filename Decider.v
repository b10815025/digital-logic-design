module button_dec(start,but0In,but1In,but2In,,but0,but1,but2,scoreout,failedout,reset,testOut, scoreOut_dec);
input start, but0In,but1In,but2In,but0,but1,but2,reset;
output failedout;
output[8:0]scoreout, testOut, scoreOut_dec;
reg failedout;
reg[8:0] scoreout,testOut, scoreOut_dec;

integer but_0, but_1 , but_2,temp,temp1, temp2,score0,score1,score2,failcount,fail0,fail1,fail2,reset_singal, reset_0,reset_singal_always, reset_1, reset_2, afterrst_0,afterrst_1,afterrst_2;
integer tmp, tmp1;

initial
begin
	temp = 0;
	temp1 = 0;
	temp2 = 0;	
end


always@ (but0In)
begin
	if(but0In)
		temp = 1;
	else
		temp = 0;
end
always@ (but1In)
begin
	if(but1In)
		temp1 = 1;
	else
		temp1 = 0;
end
always@ (but2In)
begin
	if(but2In)
		temp2 = 1;
	else
		temp2 = 0;
end


always@ (posedge reset)
begin

	if(reset_singal == 7)
	begin		
		reset_singal = 0;
	end
	else
	begin
		reset_singal = reset_singal + 1;
	end

end

always@ (posedge but0)
begin		 
	if(reset_0 != reset_singal)
	begin
		score0 = 0;
		fail0 = 0;
		reset_0 = reset_singal; 
	end
	but_0 = temp;
	if(but0 && !start)	
	begin	
		if(but_0)
			score0 = score0 + 1;	
		else
			fail0 = fail0 + 1;
	end	
end	

always@ (posedge but1)
begin
	if(reset_1 != reset_singal)
	begin
		score1 = 0;
		fail1 = 0;
		reset_1 = reset_singal; 
	end
	but_1 = temp1;
	if(but1 && !start)
	begin
		if(but_1)
			score1 = score1 + 1;	
		else
			fail1 = fail1 + 1;
	end	
end
always@ (posedge but2)
begin	
	if(reset_2 != reset_singal)
	begin
		score2 = 0;
		fail2 = 0;
		reset_2 = reset_singal; 
	end
	but_2 = temp2;
	if(but2 && !start)
	begin
		if(but_2)
			score2 = score2 + 1;	
		else 
			fail2 = fail2 + 1;
	end	
end


always
begin			
	testOut = failcount;
	tmp1 = tmp;
	tmp = reset_singal;
	if(tmp1 != tmp)
	begin
		afterrst_0 = 0;
		afterrst_1 = 0;
		afterrst_2 = 0;
	end
	if(reset_singal_always != reset_singal)
	begin
		
		if(reset_singal == reset_0)
			begin
				afterrst_0 = 1;
			end
		else
			begin
				afterrst_0 = 0;
			end
		if(reset_singal == reset_1)
			begin
				afterrst_1 = 1;
			end
		else
			begin
				afterrst_1 = 0;
			end 
		if(reset_singal == reset_2)
			begin
				afterrst_2 = 1;
			end
		else
			begin
				afterrst_2 = 0;
			end
		
		if(afterrst_0 == 1 && afterrst_1 == 1 && afterrst_2 == 1)//111
			begin
				reset_singal_always = reset_singal;
				scoreout = (score0 + score1 + score2);
				scoreOut_dec = (score0 + score1 + score2);
				failcount = fail0 + fail1 + fail2;
			end
		else if(afterrst_0 == 1 && afterrst_1 == 1 && afterrst_2 == 0)//110
		begin			
			scoreout = (score0 + score1);
			scoreOut_dec = (score0 + score1);
			failcount = (fail0 + fail1);
		end
		else if(afterrst_0 == 0 && afterrst_1 == 1 && afterrst_2 == 1)//011
		begin			
			scoreout = (score1 + score2);
			scoreOut_dec = (score1 + score2);
			failcount = (fail1 + fail2);
		end
		else if(afterrst_0 == 1 && afterrst_1 == 0 && afterrst_2 == 1)//101
		begin			
			scoreout = (score0 + score2);			
			scoreOut_dec = (score0 + score2);
			failcount = (fail0 + fail2);
		end
		else if(afterrst_0 == 1 && afterrst_1 == 0 && afterrst_2 == 0)//100
		begin			
			scoreout = (score0);
			scoreOut_dec = (score0);
			failcount = (fail0);
		end
		else if(afterrst_0 == 0 && afterrst_1 == 1 && afterrst_2 == 0)//010
		begin			
			scoreout = (score1);
			scoreOut_dec = (score1);
			failcount = (fail1);
		end
		else if(afterrst_0 == 0 && afterrst_1 == 0 && afterrst_2 == 1)//001
		begin			
			scoreout = (score2);
			scoreOut_dec = (score2);
			failcount = (fail2) ;
		end
		else 
		if(afterrst_0 == 0 && afterrst_1 == 0 && afterrst_2 == 0)
		begin			
			scoreout = 0;
			scoreOut_dec = scoreout;
			failcount = 0;
			failedout = 0;
		end
		if(failcount >= 5)
		begin
			failedout = 1;
			scoreout = scoreout;
		end
		else
			failedout = 0;
	end
	else
	begin
		afterrst_0 = 0;
		afterrst_1 = 0;
		afterrst_2 = 0;
		failcount = fail0 + fail1 + fail2;
		testOut = failcount;//test Count work
		if(failcount >= 5)
		begin
			failedout = 1;
			scoreout = scoreout;
			scoreOut_dec = scoreout;
		end
		else 
		begin			
			failedout = 0;
			scoreout = (score0 + score1 + score2);		
			scoreOut_dec = (score0 + score1 + score2);
		end	
	end	
end


endmodule
