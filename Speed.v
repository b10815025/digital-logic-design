module speed(clk_in, scoreIn, clk_out, reset, testOut);
input clk_in, reset;
input[8:0]  scoreIn;
output clk_out;
output [8:0] testOut;
reg clk_out;
reg[8:0] testOut;
integer multiple, dec, clk, temp;
initial
begin
	multiple = 10;
	dec = 2600;
	clk = 0;
end
always@ (posedge clk_in)
begin
	//testOut = dec / 100;
	temp = multiple;
	if(reset)
	begin
		multiple = 10;
		dec = 2600;		
	end
	if(scoreIn > multiple)
	begin
		multiple = scoreIn + 5;		
		dec = dec - (multiple * 10);
	end	
	if(clk < dec)
	begin
		clk <= clk + 1;
		clk_out = 1;
	end
	else
	begin 
		clk_out = 0;
		clk = 0;
	end
end
endmodule