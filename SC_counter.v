module ScoreCounter(countIn, rst ,SegOut);

input countIn, rst;
output [8:0] SegOut;
reg[8:0] SegOut;
integer rst_flag;
initial 
begin
	rst_flag = 0;
	SegOut = 0;
end
always@ (posedge countIn, posedge rst)
begin	
	if(countIn)
		SegOut = SegOut + 1;
	if(rst)
		SegOut = 0;
		
end
endmodule
