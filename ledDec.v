module leddisplay(clk, q_in, ledout);
input clk;
input [8:0]q_in;
output [9:0]ledout;
reg [9:0]ledout;

always@ (posedge clk)
begin
	if(q_in == 0)
		ledout = 9'b000000001;
	if(q_in == 1)
		ledout = 9'b000000010;
	if(q_in == 2)
		ledout = 9'b000000100;
	if(q_in == 3)
		ledout = 9'b000001000;	
	if(q_in == 4)
		ledout = 9'b000010000;
	if(q_in == 5)
		ledout = 9'b000100000;
	if(q_in == 6)
		ledout = 9'b001000000;
	if(q_in == 7)
		ledout = 9'b010000000;
	if(q_in == 8)
		ledout = 9'b100000000;
end
endmodule
	