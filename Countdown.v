module countdown(clk, rst, out, timeout, lockByFail);
input clk, rst, lockByFail;
output timeout;
output [8:0]out;
reg timeout,lock;
reg[8:0]out;
integer temp;
initial 
begin
out = 60;
temp = 0;
timeout = 0;
end

always@ (posedge clk)
begin

	if(lockByFail == 0)
	begin
	if(rst)
	begin
		out <= 60;
		temp <= 0;
		timeout = 0;
	end
	else 
	if(temp == 50000000) begin
	   out <= out - 1;
		temp <= 0;
	end
	else
	if(out == 0) begin
	out = 0;
	timeout = 1;
	end
	else
		temp <= temp + 1;
	end
	else
	out = out;
	
end

endmodule


