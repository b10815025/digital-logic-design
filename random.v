module RANDOM(q,start,clk ,button0,button1,button2, ledout,lock,scoreIn);
input start,clk,lock;
input [8:0]scoreIn;
output button0,button1,button2;
reg button0,button1,button2;
output [8:0]q;
output [9:0]ledout;
reg [8:0]q;
reg [9:0]ledout;
integer seed,delay, temp, same, check;
initial begin
   q = 0;
	seed = 1;	
	delay = 0;
	end
always@ (start)
begin
	if(start)
		check = 1;
	else
		check = 0;
end
always@ (posedge clk)
begin
delay = delay + 1;
seed = (seed << 3 + seed) % 2147123;
temp = (temp + seed * scoreIn % 8761 + (temp >> 1) % 8761 + (temp + seed) % 8761) % 2147483647;
	if(!lock)
	begin
		
		same = q;
		q = temp % 10;
		if(q == 9)
			q = 0;		
		if(q == same)
			q = 9;//let led disappear
		if(check == 0)
		begin
		if(q / 3 == 0)
		begin
			button0=1;
			button1=0;
			button2=0;
		end
		else if (q / 3 == 1)
		begin
			button0=0;
			button1=1;
			button2=0;
		end
		else
		begin
			button0=0;
			button1=0;
			button2=1;
		end
		end
		if(q == 0)
			ledout = 9'b000000001;
		if(q == 1)
			ledout = 9'b000000010;
		if(q == 2)
			ledout = 9'b000000100;
		if(q == 3)
			ledout = 9'b000001000;	
		if(q == 4)
			ledout = 9'b000010000;
		if(q == 5)
			ledout = 9'b000100000;
		if(q == 6)
			ledout = 9'b001000000;
		if(q == 7)
			ledout = 9'b010000000;
		if(q == 8)
			ledout = 9'b100000000;
		if(q == 9)
			ledout = 9'b000000000;
	end
	else	begin
		button0=0;
		button1=0;
		button2=0;
		ledout = 9'b000000000;
	end

end
endmodule
